/*
 *  Copyright 2013 Lolmewn <lolmewn@gmail.com>.
 */
package nl.lolmewn.sortal.api;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Lolmewn <lolmewn@gmail.com>
 */
public class SortalWarpCreateEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;

    private final Player sender;
    private final String name;
    private final Location location;

    public SortalWarpCreateEvent(Player sender, String name, Location location) {
        this.sender = sender;
        this.name = name;
        this.location = location;
    }

    public Player getCommandSender() {
        return sender;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean bln) {
        cancelled = bln;
    }

}
