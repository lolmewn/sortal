/*
 * Main.java
 * 
 * Copyright (c) 2012 Lolmewn <lolmewn@gmail.com>.
 * 
 * Sortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Sortal.  If not, see <http ://www.gnu.org/licenses/>.
 */
package nl.lolmewn.sortal;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import net.milkbowl.vault.economy.Economy;
import nl.lolmewn.sortal.stats.StatsProvider;
import nl.lolmewn.stats.api.StatsAPI;
import org.bstats.bukkit.Metrics;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Lolmewn <lolmewn@gmail.com>
 */
public class Main extends JavaPlugin {

    private WarpManager warpManager;
    private Settings settings;
    private MySQL mysql;
    private Economy eco; //Vault
    protected HashMap<String, Integer> setcost = new HashMap<>();
    protected HashMap<String, String> register = new HashMap<>();
    protected HashSet<String> unregister = new HashSet<>();
    protected HashMap<String, String> setuses = new HashMap<>();
    protected HashSet<String> setPrivate = new HashSet<>();
    protected HashMap<String, HashSet<String>> setPrivateUsers = new HashMap<>();
    protected double newVersion = 0;
    protected File settingsFile = new File("plugins" + File.separator + "Sortal"
            + File.separator + "settings.yml");

    @Override
    public void onDisable() {
        this.saveData();
        this.getServer().getScheduler().cancelTasks(this);
        if (this.newVersion != 0) {
            this.getSettings().addSettingToConfig(this.getSettings().settingsFile, "version", newVersion);
        }
    }

    @Override
    public void onEnable() {
        this.settings = new Settings(this); //Also loads Localisation
        if (this.getSettings().useMySQL()) {
            if (!this.initMySQL()) {
                this.getLogger().severe("Something is wrong with the MySQL database, switching to flatfile!");
                this.getSettings().setUseMySQL(false);
            }
        }
        this.warpManager = new WarpManager(this);
        this.getCommand("sortal").setExecutor(new SortalExecutor(this));
        this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
        if (!this.initVault()) {
            this.getLogger().info("Vault error or not found, setting costs to 0!");
            this.getSettings().setWarpCreatePrice(0);
            this.getSettings().setWarpUsePrice(0);
        } else {
            this.getLogger().info("Hooked into Vault and Economy plugin successfully!");
        }
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, this::saveData, 36000L, 36000L);
//        if (this.getSettings().isUpdate()) {
//            new Updater(this, "sortal", this.getFile(), Updater.UpdateType.DEFAULT, true);
//        }
        this.getServer().getPluginManager().addPermission(new Permission("sortal.warp", PermissionDefault.getByName(this.getSettings().getDefaultWarp())));
        this.getServer().getPluginManager().addPermission(new Permission("sortal.createwarp", PermissionDefault.getByName(this.getSettings().getDefaultCreateWarp())));
        this.getServer().getPluginManager().addPermission(new Permission("sortal.delwarp", PermissionDefault.getByName(this.getSettings().getDefaultDelWarp())));
        this.getServer().getPluginManager().addPermission(new Permission("sortal.list", PermissionDefault.getByName(this.getSettings().getDefaultList())));
        this.getServer().getPluginManager().addPermission(new Permission("sortal.unregister", PermissionDefault.getByName(this.getSettings().getDefaultUnregister())));
        this.getServer().getPluginManager().addPermission(new Permission("sortal.directwarp", PermissionDefault.getByName(this.getSettings().getDefaultDirectWarp())));
        this.getServer().getPluginManager().addPermission(new Permission("sortal.placesign", PermissionDefault.getByName(this.getSettings().getDefaultPlaceSign())));
        this.getServer().getPluginManager().addPermission(new Permission("sortal.setuses", PermissionDefault.getByName(this.getSettings().getDefaultSetUses())));
        hookStats();
        this.getLogger().log(Level.INFO, String.format("Version %s loaded!", this.getDescription().getVersion()));
        new Metrics(this);
    }

    protected boolean initMySQL() {
        this.mysql = new MySQL(
                this,
                this.getSettings().getDbHost(),
                this.getSettings().getDbPort(),
                this.getSettings().getDbUser(),
                this.getSettings().getDbPass(),
                this.getSettings().getDbDatabase(),
                this.getSettings().getDbPrefix());
        return !this.mysql.isFault();
    }

    public Settings getSettings() {
        return settings;
    }

    public WarpManager getWarpManager() {
        return warpManager;
    }

    public MySQL getMySQL() {
        return this.mysql;
    }

    protected String getWarpTable() {
        return this.getSettings().getDbPrefix() + "warps";
    }

    protected String getSignTable() {
        return this.getSettings().getDbPrefix() + "signs";
    }

    protected String getUserTable() {
        return this.getSettings().getDbPrefix() + "users";
    }

    protected double getVersion() {
        return this.getSettings().getVersion();
    }

    public void saveData() {
        this.getWarpManager().saveData();
    }

    public boolean pay(Player p, int amount) {
        if (amount == 0) {
            return true;
        }
        if (this.canPay(p, amount)) {
            if (!initVault()) {
                return true;
            }
            this.eco.withdrawPlayer(p, amount);
            p.sendMessage(this.getSettings().getLocalisation().getPaymentComplete(Integer.toString(amount)));
            return true;
        }
        p.sendMessage(this.getSettings().getLocalisation().getNoMoney(Integer.toString(amount)));
        return false;
    }

    public boolean canPay(Player p, int amount) {
        if (amount == 0) {
            return true;
        }
        if (initVault()) {
            return this.eco.has(p, amount);
        }
        return true;
    }

    private boolean initVault() {
        if (this.eco != null) {
            return true;
        }
        if (this.getServer().getPluginManager().getPlugin("Vault") == null) {
            //Vault not found
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = this.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            //Vault not found
            return false;
        }
        this.eco = rsp.getProvider();
        return true;
    }

    public void debug(String message) {
        if (!this.getSettings().isDebug()) {
            return;
        }
        if (!message.toLowerCase().startsWith("[debug]")) {
            this.getLogger().info("[Debug] " + message);
        } else {
            this.getLogger().info(message);
        }
    }

    public String getLocationDoubles(Location loc) {
        return loc.getWorld().getName() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ();
    }

    public String getLocationInts(Location loc) {
        return loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ();
    }

    public String getLocationDoublesPY(Location loc) {
        return loc.getWorld().getName() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getYaw() + "," + loc.getPitch();
    }

    public String getLocationIntsPY(Location loc) {
        return loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + "," + loc.getYaw() + "," + loc.getPitch();
    }

    private void hookStats() {
        Plugin stats = this.getServer().getPluginManager().getPlugin("Stats");
        if (stats == null || !stats.getDescription().getVersion().startsWith("2")) {
            return;
        }
        RegisteredServiceProvider<StatsAPI> rsp = this.getServer().getServicesManager().getRegistration(StatsAPI.class);
        if (rsp == null) return;
        new StatsProvider(this, rsp.getProvider());
    }
}
