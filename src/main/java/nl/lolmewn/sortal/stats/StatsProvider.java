package nl.lolmewn.sortal.stats;

import nl.lolmewn.sortal.Main;
import nl.lolmewn.sortal.api.SortalPlayerTeleportEvent;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatTableType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 * @author Sybren
 */
public class StatsProvider implements Listener {

    private final StatsAPI api;
    private final Stat tps;

    public StatsProvider(Main main, StatsAPI api) {
        this.api = api;
        String tableName = api.getDatabasePrefix() + "sortal";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, true, api.isCreatingSnapshots()));
        }
        StatsTable table = api.getStatsTable(tableName);
        main.getServer().getPluginManager().registerEvents(this, main);
        tps = api.addStat("Sortal tps", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "tps", null);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerTeleport(SortalPlayerTeleportEvent event) {
        StatsPlayer player = api.getPlayer(event.getPlayer());
        player.getStatData(tps, event.getSignInfo().getWorld(), true).addUpdate(new Object[]{}, 1);
    }
}
